﻿/**************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the Qt Installer Framework.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** As a special exception, The Qt Company gives you certain additional
** rights. These rights are described in The Qt Company LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
**
** $QT_END_LICENSE$
**
**************************************************************************/

function Component()
{
    // default constructor
}

Component.prototype.createOperations = function()
{
    // call default implementation to actually install README.txt!
    component.createOperations();

    if (systemInfo.productType === "windows") {
		//创建快捷方式
        component.addOperation("CreateShortcut", "@TargetDir@\\CassNotification.exe", "@StartMenuDir@\\开思提醒.lnk",
            "workingDirectory=@TargetDir@");
                component.addOperation("CreateShortcut", "@TargetDir@\\CassNotification.exe", "@DesktopDir@\\开思提醒.lnk",
            "workingDirectory=@TargetDir@");
		//文件夹内创建一个快捷方式，用于启动
                component.addOperation("CreateShortcut","@TargetDir@\\CassNotification.exe","@TargetDir@\\开思提醒.lnk","workingDirectory=@TargetDir@");
		//开机启动
		component.addOperation("GlobalConfig","HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run","@Title@","@TargetDir@\\开思提醒.lnk");
    }
}

Component.prototype.installationFinished = function()
{

}

Component.prototype.uninstallationFinished = function(){
	try {
		if(installer.isUninstaller() && installer.status == QInstaller.Success){
                        //开机启动
                        component.addOperation("GlobalConfig","HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run","@Title@","");
			component.addOperation("Execute", "del","@TargetDir@\\开思提醒.lnk");
			component.addOperation("Execute", "del","@DesktopDir@\\开思提醒.lnk");
		}
    } catch(e) {
        console.log(e);
    }
}
