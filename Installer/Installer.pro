TEMPLATE = aux
INSTALLER = installer
DISTFILES += \
    config/config.xml \
    packages/com.cassmall.destop.remind/meta/package.xml \
    packages/com.cassmall.destop.remind/meta/installscript.qs \
    packages/com.cassmall.destop.remind/meta/license.txt \
    create-setup.bat \
    gen-repo.bat \
    packages/com.cassmall.destop.main/meta/package.xml \
    packages/com.cassmall.destop.main/meta/installscript.qs \
    packages/com.cassmall.destop.main/meta/license.txt

