QT += core
QT -= gui

CONFIG += c++11

TARGET = casscfg
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

DESTDIR += ../bin

SOURCES += main.cpp

win32:RC_FILE = resource.rc
