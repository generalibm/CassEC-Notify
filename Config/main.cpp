﻿#include <QCoreApplication>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QDebug>
#include <QTextStream>
#include <QSettings>
#include <QDebug>
#include <QDir>
#include <QStandardPaths>
//输出
QTextStream& qStdOut()
{
    static QTextStream ts( stdout );
    return ts;
}

//设置可执行程序的开机启动，或取消开机启动
void setStartupOption(QString exePath,bool startWithComputer){

    QSettings *reg=new QSettings("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",
                                 QSettings::NativeFormat);
    //开机自动运行
    if (startWithComputer)
    {
        exePath.replace("/","\\");
        reg->setValue("开思提醒",exePath);
        qDebug()<<exePath;
    } else {
        reg->remove("开思提醒");
    }

}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QCommandLineParser parser;
    parser.setApplicationDescription(QStringLiteral("开思商城桌面配置: casscfg"));

    parser.addHelpOption();
    QCommandLineOption startOption({"s","start-with-computer"},QStringLiteral("是否开机启动?"),QString("start-value"));
    QCommandLineOption linkOption({"l","link"},QStringLiteral("创建快捷方式到桌面"));
    parser.addOption(startOption);
    parser.addOption(linkOption);
    parser.process(a);

    if(parser.isSet(startOption)){
        QString value = parser.value(startOption);
        qStdOut()<<"value is "<<value;
        QString path = a.applicationDirPath();
        QDir dir(path);
        QString exePath= dir.absoluteFilePath("CassNotification.exe");
        qStdOut()<<path;
        if(value == "true"){
            setStartupOption(exePath,true);
        } else if(value == "false"){
            setStartupOption(exePath,false);
        } else {
            qStdOut()<<"SET_STARTUP_OPTION:error";
        }
    }
    if(parser.isSet(linkOption)){
        QString desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
        QDir dir(desktopPath);
        QString linkPath = dir.absoluteFilePath(QStringLiteral("开思提醒.lnk"));
        bool ret = QFile::link(QString("./Main.exe"),QString(linkPath));
        if(!ret){
            qDebug("CREATE_LINK:error");
        }
    }


    return 0;
}
