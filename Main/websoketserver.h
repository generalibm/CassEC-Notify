﻿#ifndef WEBSOKETSERVER_H
#define WEBSOKETSERVER_H

#include "cookiejar.h"

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QObject>
#include <QWebSocket>

class WebSocketServer : public QObject
{
    Q_OBJECT
public:
    explicit WebSocketServer(QObject *parent = 0);
    void setUserPassword(const QString &username, const QString &password = QString());

    QString getErrorMessage() const;

    QString getToken() const;
    void setToken(const QString &value);

    QString getUsername() const;

private:
    QWebSocket websoket;
    QNetworkAccessManager *manager;

    QString username;
    QString password;

    QString errorMessage;
    int retried;

    QString token;

    CookieJar cookieJar;
    //用于保存登录的JSESSIONID
    QString session;

    bool isLogin;

    QAbstractSocket::SocketError currentError;

signals:
    void messageRecived(QString msg,QString link);
    void updateRequest();
    void connectSuccess();
    void connectFailed(QString);

public slots:
    void connectServer();
    void login(const QString &username, const QString &password);

private slots:
    void doTextMessage(const QString &message);
    void replyFinished(QNetworkReply* reply);
    void doConnnected();
    void doDisconnected();
    void doError(QAbstractSocket::SocketError error);
};
#endif // WEBSOKETSERVER_H
