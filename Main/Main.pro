#-------------------------------------------------
#
# Project created by QtCreator 2016-03-07T13:24:21
#
#-------------------------------------------------

QT       += core gui websockets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CassNotification
TEMPLATE = app

DESTDIR += ../bin

SOURCES += mainwindow.cpp \
    systemtrayicon.cpp \
    logindialog.cpp \
    remindwidget.cpp \
    application.cpp \
    websoketserver.cpp \
    main.cpp \
    settings.cpp \
    cookiejar.cpp

HEADERS  += mainwindow.h \
    systemtrayicon.h \
    logindialog.h \
    remindwidget.h \
    application.h \
    websoketserver.h \
    settings.h \
    cookiejar.h

RESOURCES += \
    resource.qrc

RC_ICONS = main.ico

