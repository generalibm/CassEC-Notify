﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "logindialog.h"
#include "remindwidget.h"
#include "systemtrayicon.h"
#include "settings.h"
#include "websoketserver.h"
#include <QMainWindow>
#include <QSettings>


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    //托盘图标
    SystemTrayIcon *trayIcon;

    //登录框
    LoginDialog *loginDialog;

    //提醒框
    RemindWidget *remindWidget;


    WebSocketServer *websocketServer;

    // 配置文件
    Settings *settings;



public slots:
    void showNewAppMessage();
    void login(const QString& username, const QString &password,bool isRemember);
    void checkUpdate();
    void updateSoftware();
    void clickMessage();
    QString maintainToolPath();
    void openUrl(QString url);
};

#endif // MAINWINDOW_H
