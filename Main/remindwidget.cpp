﻿#include "remindwidget.h"

#include <QDesktopWidget>
#include <QApplication>
#include <QLabel>
#include <QDebug>
#include <QGraphicsDropShadowEffect>
#include <QPropertyAnimation>
#include <QTimer>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonParseError>

const int WIDGET_WIDTH = 305;
const int WIDGET_HEIGHT = 189;

RemindWidget::RemindWidget(QWidget *parent)
    :QWidget(parent)
{

    background = new QLabel(this);  //背景
    background->setObjectName("remind-background");
    background->setFixedSize(WIDGET_WIDTH,WIDGET_HEIGHT);
    background->move(5,5);

    // 登录界面设置阴影效果
    QGraphicsDropShadowEffect *shadowEffect = new QGraphicsDropShadowEffect(this);
    shadowEffect->setOffset(3, 3);
    shadowEffect->setColor(QColor("#666"));
    shadowEffect->setBlurRadius(10);
    background->setGraphicsEffect(shadowEffect);

    //标题
    titleBar = new QLabel(background);
    titleBar->setObjectName("remind-title-bar");
    titleBar->setFixedSize(WIDGET_WIDTH,32);

    //图标
    QLabel *icon = new QLabel(titleBar);
    icon->setObjectName("remind-title-icon");
    icon->setFixedSize(30,32);
    icon->move(0,0);
    //标题
    QLabel *title = new QLabel(QStringLiteral("开思·温馨提醒"),titleBar);
    title->setFixedHeight(32);
    title->setObjectName("remind-title-text");
    title->move(30,0);

    closeButton = new QToolButton(titleBar);
    closeButton->setObjectName("remind-close-button");
    closeButton->setFixedSize(32,32);
    closeButton->setIcon(QIcon(":/res/img/close.png"));
    closeButton->setIconSize(QSize(12,12));
    closeButton->move(WIDGET_WIDTH-32,0);
    closeButton->setFocusPolicy(Qt::NoFocus);

    //点击关闭，隐藏窗口
    connect(closeButton,SIGNAL(clicked()),this,SLOT(hideGradient()));

    //内容
    content = new QLabel(background);
    content->setObjectName("remind-content");
    content->setGeometry(0, 32, WIDGET_WIDTH, WIDGET_HEIGHT - 32 - 36);
    connect(content,SIGNAL(linkActivated(QString)),this,SIGNAL(urlClicked(QString)));

    //底部,查看详情
    footerBar = new QLabel(background);
    footerBar->setObjectName("remind-footer");
    footerBar->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
    footerBar->setFixedSize(WIDGET_WIDTH,36);
    footerBar->move(0, WIDGET_HEIGHT - 36);
    connect(footerBar,SIGNAL(linkActivated(QString)),this,SIGNAL(urlClicked(QString)));


    // 无边框，透明
    this->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowSystemMenuHint| Qt::Tool | Qt::WindowStaysOnTopHint);
    this->setAttribute(Qt::WA_NoSystemBackground, true);
    this->setAttribute(Qt::WA_TranslucentBackground,true);


    //固定大小
    this->setFixedSize(WIDGET_WIDTH + 10, WIDGET_HEIGHT + 10);
    this->fixedPosition();
}

void RemindWidget::fixedPosition()
{
    QDesktopWidget *desktop = QApplication::desktop();
    QRect desktopRect = desktop->availableGeometry();

    const int bottom = 0;
    const int right = 0;

    int left = desktopRect.width() - this->width() - right;
    int top = desktopRect.height() -this->height() - bottom;

    this->move(left,top);
}


QString RemindWidget::getMessageLink() const
{
    return messageLink;
}


void RemindWidget::showRemind(const QString &msg, const QString &link)
{
    qDebug()<<msg;
    content->setText("<a href=\"" + link + "\" style=\"color:#555;text-decoration:none;\">" + msg + "</a>");
    footerBar->setText("<a href=\""+ link +"\" style=\"color:#2d9dd7;text-decoration:none;\">立即查看 <b>></b> </a>");
    showGradient();
}

void RemindWidget::showGradient()
{
    //如果是隐藏的，渐变出场
    if(this->isHidden()){
        this->show();
        QPropertyAnimation *animation = new QPropertyAnimation(this,"windowOpacity");
        animation->setStartValue(0.0);
        animation->setEndValue(1.0);
        animation->setDuration(500);
        animation->start();
        connect(animation,SIGNAL(finished()),animation,SLOT(deleteLater()));
    }
}

void RemindWidget::hideGradient()
{
    //如果是隐藏的，渐变出场
    if(!this->isHidden()){
        this->show();

        QPropertyAnimation *animation = new QPropertyAnimation(this,"windowOpacity");
        animation->setStartValue(1.0);
        animation->setEndValue(0);
        animation->setDuration(500);
        animation->start();
        connect(animation,SIGNAL(finished()),this,SLOT(hide()));
        connect(animation,SIGNAL(finished()),animation,SLOT(deleteLater()));
    }

}

void RemindWidget::mousePressEvent(QMouseEvent *event)
{
    this->windowPos = this->pos();
    this->mousePos = event->globalPos();
    this->deltaPos = mousePos - windowPos;
    this->isPressed = true;
}

void RemindWidget::mouseMoveEvent(QMouseEvent *event)
{
    if(this->isPressed)
        this->move(event->globalPos() - this->deltaPos);
}

void RemindWidget::mouseReleaseEvent(QMouseEvent *)
{
    this->isPressed = false;
}
