﻿#ifndef APPLICATION_H
#define APPLICATION_H

#include <QApplication>
#include <QLocalServer>



class Application : public QApplication
{
    Q_OBJECT
public:
    Application(int argc,char **argv);
private:
    QLocalServer *server;

    void loadStylesheet();
signals:
    void newInstanceBoot();
private slots:
    void newLocalConnection();
};

#endif // APPLICATION_H
