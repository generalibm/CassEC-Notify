﻿#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>

// 全局配置文件，单例
class Settings : public QObject
{
    Q_OBJECT
public:
    static Settings *getInstance(){
        if(settings == NULL){
            settings = new Settings(NULL);
        }
        return settings;
    }

    void setPassword(const QString &password);
    void setUsername(const QString &username);
    void setRememberState(bool isRemember);
    QString getPassword() const;
    QString getUsername() const;
    bool getRememberState() const;
    QString getAppName() const;
    QString getAppVersion() const;
private:
    static Settings *settings;
    Settings(QObject *parent = 0);

    //配置文件
    QSettings *iniSettings;

signals:

public slots:
};


#endif // SETTINGS_H
