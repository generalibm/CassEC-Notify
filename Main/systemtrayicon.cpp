﻿#include "systemtrayicon.h"

#include <QApplication>
#include <QMenu>
#include <QDebug>
SystemTrayIcon::SystemTrayIcon(QObject *parent)
    :QSystemTrayIcon(parent)
{
    QMenu* trayMenu = new QMenu;

    homePageAction = new QAction(QStringLiteral("个人中心"),this);
    viewMsgAction = new QAction(QStringLiteral("查看消息"),this);
    exitAction = new QAction(QStringLiteral("退出"),this);

    connect(this->exitAction,SIGNAL(triggered()),this,SLOT(exit()));

    trayMenu->addAction(viewMsgAction);
    trayMenu->addAction(homePageAction);
    trayMenu->addAction(exitAction);

    this->setContextMenu(trayMenu);

    this->setIcon(QIcon("://res/img/logo.png"));
}

SystemTrayIcon::MessageType SystemTrayIcon::getCurrentMessageType() const
{
    return currentMessageType;
}

QString SystemTrayIcon::getCurrentMessage() const
{
    return currentMessage;
}



void SystemTrayIcon::exit()
{
    QApplication::quit();
}

void SystemTrayIcon::showWelcome()
{
    this->show();
    //this->showMessage(SystemTrayIcon::NORMAL_MESSAGE,QStringLiteral("欢迎使用开思提醒客户端"));
}


void SystemTrayIcon::showMessage(SystemTrayIcon::MessageType type, const QString &message)
{
    this->currentMessage = message;
    this->currentMessageType = type;
    switch (type) {
    case UPDATE_MESSAGE:
        QSystemTrayIcon::showMessage(QStringLiteral("软件升级"), message,QSystemTrayIcon::NoIcon,5000);
        break;
    case NORMAL_MESSAGE:
        QSystemTrayIcon::showMessage(QStringLiteral("开思提醒"),message,QSystemTrayIcon::NoIcon,5000);
    default:
        break;
    }
}
