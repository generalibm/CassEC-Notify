﻿#ifndef COOKIEJAR_H
#define COOKIEJAR_H

#include <QNetworkCookie>
#include <QNetworkCookieJar>



class CookieJar : public QNetworkCookieJar
{
public:
    CookieJar(QObject *parent = 0);
    QList<QNetworkCookie> getCookies();
    void setCookies(const QList<QNetworkCookie>& cookieList);
};

#endif // COOKIEJAR_H
