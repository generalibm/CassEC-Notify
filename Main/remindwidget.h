﻿#ifndef REMINDWIDGET_H
#define REMINDWIDGET_H

#include <QLabel>
#include <QMouseEvent>
#include <QToolButton>
#include <QWidget>



class RemindWidget:public QWidget
{
    Q_OBJECT
signals:
     void urlClicked(QString url);
public:
    RemindWidget(QWidget *parent=0);
    QString getMessageLink() const;

private:

    QLabel *background;  //背景
    QLabel *titleBar;  //标题背景
    QLabel *footerBar;//底部背景
    QLabel *content;    //提醒内容
    QToolButton *closeButton; //关闭按钮

    QPoint windowPos;   // 窗口坐标
    QPoint mousePos;    //鼠标坐标
    QPoint deltaPos;    //相对移动坐标
    bool isPressed;

    void fixedPosition();
    QString messageLink;


public slots:
    void showRemind(const QString &msg,const QString &link);
    void showGradient();
    void hideGradient();

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *);

};

#endif // REMINDWIDGET_H
