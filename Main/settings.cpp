﻿#include "settings.h"
#include<QString>
const QString CONFIG_FILE_PATH = QStringLiteral("cfg.dat");

//初始静态函数为NULL
Settings* Settings::settings = NULL;

Settings::Settings(QObject *parent) : QObject(parent)
{
    iniSettings = new QSettings(CONFIG_FILE_PATH,QSettings::IniFormat);
}

void Settings::setPassword(const QString &password)
{
    iniSettings->setValue(QByteArray("PASSWORD").toHex(),password.toLocal8Bit().toBase64());
}

void Settings::setUsername(const QString &username)
{
    iniSettings->setValue(QByteArray("USERNAME").toHex(),username.toLocal8Bit().toBase64());
}

void Settings::setRememberState(bool isRemember)
{
    iniSettings->setValue(QByteArray("REMEMBER").toHex(),isRemember);
}

QString Settings::getPassword() const
{
    QString password =  QByteArray::fromBase64(iniSettings->value(QByteArray("PASSWORD").toHex()).toByteArray());
    return password;
}

QString Settings::getUsername() const
{
    QString username = QByteArray::fromBase64(iniSettings->value(QByteArray("USERNAME").toHex()).toByteArray());
    return username;
}

bool Settings::getRememberState() const
{
    QString remember = iniSettings->value(QByteArray("REMEMBER").toHex()).toString();
    bool isRemember = remember == "false" ? false : true;
    return isRemember;
}

QString Settings::getAppName() const
{
    return iniSettings->value("APP_NAME",QStringLiteral("开思提醒")).toString();
}

QString Settings::getAppVersion() const
{
    return iniSettings->value("APP_VERSION").toString();
}

