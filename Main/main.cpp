﻿#include "application.h"
#include "mainwindow.h"

#include <QFile>
#include <QDebug>

//自定义日志输出，debug()输出到控制台，其他输出到日志文件
void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    QDateTime now = QDateTime::currentDateTime();

    FILE *logoutput = fopen("cass.log","a");
    QString nowStr = now.toString("yyyy-MM-dd hh:mm:ss.zzz");
    switch (type) {
    case QtDebugMsg:
        fprintf(stdout, "Debug:[%s](%s) %s \n",nowStr.toLocal8Bit().constData(),context.category, localMsg.constData());
        fflush(stdout);
        break;
    case QtInfoMsg:
        fprintf(logoutput, "Info:[%s](%s) %s \n", nowStr.toLocal8Bit().constData(),context.category, localMsg.constData());
        break;
    case QtWarningMsg:
        fprintf(logoutput, "Warning:[%s](%s) %s \n",  nowStr.toLocal8Bit().constData(),context.category, localMsg.constData());
        break;
    case QtCriticalMsg:
        fprintf(logoutput, "Critical:[%s](%s) %s \n", nowStr.toLocal8Bit().constData(),context.category,localMsg.constData());
        break;
    case QtFatalMsg:
        fprintf(logoutput, "Fatal:[%s](%s) %s \n", nowStr.toLocal8Bit().constData(),context.category,  localMsg.constData());
        abort();
    }
    fclose(logoutput);
}
int main(int argc, char *argv[])
{
    qInstallMessageHandler(myMessageOutput);
    qDebug()<<"boot";
    Application app(argc, argv);
    MainWindow window;
    QObject::connect(&app,SIGNAL(newInstanceBoot()),&window,SLOT(showNewAppMessage()));
    //window.show(); 不显示主widget
    return app.exec();
}
