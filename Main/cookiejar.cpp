﻿#include "cookiejar.h"

CookieJar::CookieJar(QObject *parent)
    :QNetworkCookieJar(parent)
{

}

QList<QNetworkCookie> CookieJar::getCookies()
{
    return allCookies();
}

void CookieJar::setCookies(const QList<QNetworkCookie> &cookieList)
{
    setAllCookies(cookieList);
}
