﻿#include "logindialog.h"
#include <QGraphicsDropShadowEffect>
#include <QMouseEvent>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDesktopServices>
#include <QToolButton>
#include <QApplication>
#include <QToolTip>
LoginDialog::LoginDialog(QWidget *parent)
    :QDialog(parent)
{
    isPressed = false;
    this->setupUi();
    this->initSignal();
}

void LoginDialog::setDefaultInput(const QString &username, const QString &password, bool isRemember)
{
    usernameInput->setText(username);
    passwordInput->setText(password);
    rememberPasswordCheckBox->setChecked(isRemember);
}

//初始化UI
void LoginDialog::setupUi()
{
    this->setFixedWidth(470);
    this->setFixedHeight(500);


    //背景label
    backgroundLabel = new QLabel(this);
    backgroundLabel->setObjectName("login-background");
    backgroundLabel->setFixedSize(462,400);
    backgroundLabel->move(0,64);

    // 登录界面设置阴影效果
    QGraphicsDropShadowEffect *shadowEffect = new QGraphicsDropShadowEffect(this);
    shadowEffect->setOffset(2, 2);
    shadowEffect->setColor(QColor("#666"));
    shadowEffect->setBlurRadius(5);
    backgroundLabel->setGraphicsEffect(shadowEffect);


    //关闭按钮
    closeButton = new QToolButton(backgroundLabel);
    closeButton->setObjectName("login-close-button");
    closeButton->setIcon(QIcon(":/res/img/close.png"));
    closeButton->setFixedSize(32,32);
    closeButton->move(462-32,0);

    //最小化按钮
    minButton = new QToolButton(backgroundLabel);
    minButton->setObjectName("login-min-button");
    minButton->setIcon(QIcon(":/res/img/min.png"));
    minButton->setFixedSize(32,32);
    minButton->move(462-32-32,0);

    // 登录logo
    loginLogo = new QLabel(this);
    loginLogo->setObjectName("login-logo");
    loginLogo->setFixedSize(168,168);
    loginLogo->move(168,0);


    // 用户名输入框
    usernameInput = new QLineEdit(this);
    usernameInput->setPlaceholderText(QStringLiteral("用户名"));
    usernameInput->setObjectName("login-user");

    // 密码输入框
    passwordInput = new QLineEdit(this);
    passwordInput->setPlaceholderText(QStringLiteral("密  码"));
    passwordInput->setEchoMode(QLineEdit::Password);
    passwordInput->setObjectName("login-password");

    //记住密码，忘记密码
    rememberPasswordCheckBox = new QCheckBox(QStringLiteral("记住密码？"),backgroundLabel);
    passwordForgotLabel = new QLabel(QStringLiteral("<a style='color:#999;text-decoration:none;font-size:14px;' href='#forgotPassword'>忘记密码？</a>"),backgroundLabel);
    rememberPasswordCheckBox->setFixedHeight(25);
    passwordForgotLabel->setFixedHeight(25);

    QHBoxLayout *optionLayout = new QHBoxLayout();
    optionLayout->addWidget(rememberPasswordCheckBox);
    optionLayout->addStretch();
    optionLayout->addWidget(passwordForgotLabel);
    optionLayout->setContentsMargins(20,0,20,0);

    //找回密码，注册账号
    footerLabel = new QLabel(QStringLiteral("<a style='color:#999;text-decoration:none;font-size:14px;'"
                                            " href='#findPassword'>找回密码</a> "
                                            "<a style='color:#999;font-size:14px;'>|</a> "
                                            "<a style='color:#999;font-size:14px;' >注册账号</a>"),backgroundLabel);
    footerLabel->setFixedHeight(20);

    QHBoxLayout *footerLayout = new QHBoxLayout();
    footerLayout->addStretch();
    footerLayout->addWidget(footerLabel);
    footerLayout->setMargin(0);

    // 登录按钮
    loginButton = new QPushButton(QStringLiteral("登录"),this);
    loginButton->setObjectName("login-button");


    // 将控件添加到布局
    QVBoxLayout* mainLayout = new QVBoxLayout(backgroundLabel);
    mainLayout->addWidget(usernameInput);
    mainLayout->addWidget(passwordInput);
    mainLayout->addLayout(optionLayout);
    mainLayout->addWidget(loginButton);
    mainLayout->addLayout(footerLayout);
    mainLayout->setContentsMargins(35,0,30,0);

    backgroundLabel->setLayout(mainLayout);

    // 无边框，透明
    this->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowSystemMenuHint|Qt::WindowMinMaxButtonsHint|Qt::WindowCloseButtonHint);
    this->setAttribute(Qt::WA_NoSystemBackground, true);
    this->setAttribute(Qt::WA_TranslucentBackground,true);
}

void LoginDialog::initSignal()
{
    //最小化和关闭按钮
    connect(minButton,SIGNAL(clicked()),this,SLOT(showMinimized()));
    connect(closeButton,SIGNAL(clicked()),this,SLOT(close()));

    //所有<a>标签跳转到openAnchor();根据锚点判断跳转链接
    connect(passwordForgotLabel,SIGNAL(linkActivated(QString)),this,SLOT(openAnchor(QString)));
    connect(footerLabel,SIGNAL(linkActivated(QString)),this,SLOT(openAnchor(QString)));

    //登录按钮
    connect(loginButton,SIGNAL(clicked()),this,SLOT(doLogin()));
}

void LoginDialog::mousePressEvent(QMouseEvent *event)
{
    this->windowPos = this->pos();
    this->mousePos = event->globalPos();
    this->deltaPos = mousePos - windowPos;
    this->isPressed = true;
}

void LoginDialog::mouseMoveEvent(QMouseEvent *event)
{
    if(this->isPressed)
        this->move(event->globalPos() - this->deltaPos);
}

void LoginDialog::mouseReleaseEvent(QMouseEvent *)
{
    this->isPressed = false;
}

void LoginDialog::closeEvent(QCloseEvent *)
{
    QApplication::quit();
}

void LoginDialog::doLogin()
{
    if(usernameInput->text().trimmed().isEmpty()){

        QPoint userinputPoint =  this->pos() + backgroundLabel->pos() + QPoint(330,150);
        QToolTip::showText(userinputPoint,QStringLiteral("用户名不能为空"));
        return;
    }
    if(passwordInput->text().isEmpty()){
        QPoint userinputPoint =  this->pos() + backgroundLabel->pos() + QPoint(330,210);
        QToolTip::showText(userinputPoint,QStringLiteral("密码不能为空"));
        return;
    }
    loginButton->setDisabled(true);
    passwordInput->setDisabled(true);
    usernameInput->setDisabled(true);
    loginButton->setText(QStringLiteral("登录中..."));
    emit readyToLogin(usernameInput->text().trimmed(),passwordInput->text(),rememberPasswordCheckBox->isChecked());
}

void LoginDialog::showFailedMessage(const QString &msg)
{
    loginButton->setDisabled(false);
    passwordInput->setDisabled(false);
    usernameInput->setDisabled(false);
    loginButton->setText(QStringLiteral("登录"));
    this->show();
    QPoint point =  this->pos() + backgroundLabel->pos() + QPoint(180,140);
    QToolTip::showText(point,msg);
}

void LoginDialog::openAnchor(const QString &anchor)
{
    if("#forgotPassword" == anchor){
        QDesktopServices::openUrl(QUrl("http://ec.casstime.com/portal/lookForPassword"));
    }
    else if("#findPassword" == anchor){
        QDesktopServices::openUrl(QUrl("http://ec.casstime.com/portal/lookForPassword"));
    }
    else if("#register" == anchor){
        //暂未开放
    }
    else{
        QDesktopServices::openUrl(QUrl(anchor));
    }
}
