﻿#ifndef SYSTEMTRAYICON_H
#define SYSTEMTRAYICON_H

#include <QAction>
#include <QSystemTrayIcon>



class SystemTrayIcon : public QSystemTrayIcon
{
    Q_OBJECT
public:
    SystemTrayIcon(QObject *parent=0);

    enum MessageType{
        UPDATE_MESSAGE,
        NORMAL_MESSAGE
    };


    MessageType getCurrentMessageType() const;

    QString getCurrentMessage() const;

private:
    QAction *homePageAction;    //个人中心
    QAction *viewMsgAction;      //查看消息
    QAction *exitAction;        //退出
    QString currentMessage;     //当前消息
    MessageType currentMessageType;


public slots:
    void exit();
    void showWelcome();
    void showMessage(MessageType type, const QString &message);
};

#endif // SYSTEMTRAYICON_H
