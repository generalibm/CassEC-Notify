﻿#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QPushButton>
#include <QDialog>
#include<QLineEdit>
#include <QLabel>
#include <QCheckBox>

class LoginDialog : public QDialog
{
    Q_OBJECT
public:
    LoginDialog(QWidget *parent = 0);
    void setDefaultInput(const QString &username, const QString &password, bool isRemember = true);

public slots:
    void doLogin();
    void showFailedMessage(const QString &msg);
signals:
    void readyToLogin(QString username, QString password, bool isRemember);

private:
    QLabel *backgroundLabel;

    QToolButton *minButton;
    QToolButton *closeButton;

    QLineEdit *usernameInput;
    QLineEdit *passwordInput;
    QPushButton *loginButton;
    QLabel *loginLogo;

    QCheckBox *rememberPasswordCheckBox;
    QLabel *passwordForgotLabel;
    QLabel *footerLabel;

    QPoint windowPos;   // 窗口坐标
    QPoint mousePos;    //鼠标坐标
    QPoint deltaPos;    //相对移动坐标
    bool isPressed;

    // 初始化UI
    void setupUi();
    // 链接信号
    void initSignal();
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *);
    void closeEvent(QCloseEvent *);

private slots:
    void openAnchor(const QString &anchor);
};

#endif // LOGINDIALOG_H
